package dbClient

import (
	"google.golang.org/grpc/grpclog"
	"google.golang.org/grpc/credentials"
	"log"
	"sync"
	"flag"
	play "playyServer"
	db "playyDbServer"
	"google.golang.org/grpc"
	"golang.org/x/net/context"
	"errors"
)

var (
	tls                = flag.Bool("tls", false, "Connection uses TLS if true, else plain TCP")
	caFile             = flag.String("ca_file", "testdata/ca.pem", "The file containning the CA root cert file")
	serverAddr         = flag.String("server_addr", "127.0.0.1:10000", "The server address in the format of host:port")
	serverHostOverride = flag.String("server_host_override", "x.test.youtube.com", "The server name use to verify the hostname returned by TLS handshake")
)

type PlayDbClient struct {
	client_lock *sync.Mutex
	client db.PlayDbServiceClient
}

func newRPCConn() (*grpc.ClientConn, error) {

	flag.Parse()
	var opts []grpc.DialOption
	if *tls {
		var sn string
		if *serverHostOverride != "" {
			sn = *serverHostOverride
		}
		var creds credentials.TransportAuthenticator
		if *caFile != "" {
			var err error
			creds, err = credentials.NewClientTLSFromFile(*caFile, sn)
			if err != nil {
				grpclog.Fatalf("Failed to create TLS credentials %v", err)
			}
		} else {
			creds = credentials.NewClientTLSFromCert(nil, sn)
		}
		opts = append(opts, grpc.WithTransportCredentials(creds))
	} else {
		opts = append(opts, grpc.WithInsecure())
	}
	conn, err := grpc.Dial(*serverAddr, opts...)

	if err != nil {
		grpclog.Fatalf("fail to dial: %v", err)
	}

	return conn, err
}

func NewPlayDbClient() *PlayDbClient {
	new_cli := new(PlayDbClient)
	conn, err := newRPCConn()

	if err != nil {
		if conn_status, err := conn.State(); (err != nil && conn_status != grpc.Ready) {
			log.Println("Failed to connect")
			return nil
		}
	}

	new_cli.client = db.NewPlayDbServiceClient(conn)
	new_cli.client_lock = new(sync.Mutex)

	return new_cli
}

func (db_client *PlayDbClient) refreshRPCConn()  {

	if _, err := db_client.client.GetHeartbeat(context.Background(), &db.EmptyMessage{}); err != nil {
		conn, err := newRPCConn()
		if err != nil {
			log.Fatal("Failed to connect")
		}
		db_client.client = db.NewPlayDbServiceClient(conn)
	}
}

//Each call to GetSyncDbClient should be followed by Release.
func (db_client *PlayDbClient) getSyncDbClient() db.PlayDbServiceClient {
	db_client.client_lock.Lock()
	db_client.refreshRPCConn()
	return db_client.client
}

func (db_client *PlayDbClient) releaseDbClient() {
	db_client.client_lock.Unlock()
}

// Will translate from playyDbServer messages to playyServer messages.
// All *_in parameters are pre checked.
func (db_client *PlayDbClient) GetUserByInfoReq(c context.Context, user_in *play.UserInfo) (user_out *play.UserInfo, err_out error) {

	_db := db_client.getSyncDbClient()
	resp, err := _db.GetUserByInfo(c, copyUserInfo(user_in))
	db_client.releaseDbClient()
	if err != nil {
		err_out = err
		return
	}
	user_out = copyUserInfoBack(resp)
	return
}

func (db_client *PlayDbClient) CreateUserReq(c context.Context, user_in *play.UserInfo) (user_out *play.UserInfo, err_out error) {

	_db:= db_client.getSyncDbClient()
	resp, err := _db.CreateUser(c, copyUserInfo(user_in))
	db_client.releaseDbClient()
	if err != nil {
		err_out = err
		return
	}
	user_out = copyUserInfoBack(resp)
	return
}

func (db_client *PlayDbClient) GetMyEventsReq(c context.Context, uuid_in *play.ReqUUID) (events_out *play.Events, err_out error) {

	_db:= db_client.getSyncDbClient()
	p, err := _db.GetEventsParticipationByUserId(c, copyUUID(uuid_in))
	if err != nil || len(p.MyEvent) == 0 {
		if err == nil {
			err_out = errors.New("Empty response")
		} else {
			err_out = err
		}
		db_client.releaseDbClient()
		return
	}
	events_out = new(play.Events)
	//Only go ahead with successful lookups.
	for i := range p.MyEvent {
		if ev, e := _db.GetEventById(c, &db.ReqUUID{Uuid:p.MyEvent[i].EventUuid}); e == nil {
			events_out.Events = append(events_out.Events, copyEventBack(ev))

			if vn, e := _db.GetVenueById(c, &db.ReqUUID{Uuid:ev.VenueUuid}); e == nil {
				events_out.Events[len(events_out.Events) - 1].Venue = copyVenueBack(vn)
			}

			if org, e := _db.GetOrganizerById(c, &db.ReqUUID{Uuid:ev.OrganizerUuid}); e == nil {
				events_out.Events[len(events_out.Events) - 1].Org = copyOrgBack(org)
			}

			if ea, e := _db.GetEventAvailabilityInfo(c, &db.ReqUUID{Uuid:ev.EventUuid}); e == nil {
				events_out.Events[len(events_out.Events) - 1].EaInfo = copyEventAvailabilityBack(ea)
			}

			if ep, e := _db.GetEventPlayersInfo(c, &db.ReqUUID{Uuid:ev.EventUuid}); e == nil {
				events_out.Events[len(events_out.Events) - 1].EpInfo = copyEventPlayersBack(ep)
			}
		}
	}
	db_client.releaseDbClient()
	return
}

func (db_client *PlayDbClient) GetVenuesByLocationReq(c context.Context, loc_in *play.LocationInfo) (venues_out *play.Venues, err_out error) {

	_db := db_client.getSyncDbClient()
	venues, err := _db.GetVenuesByLocation(c, copyLocInfo(loc_in))
	if err != nil || len(venues.Venues) == 0 {
		if err == nil {
			err_out = errors.New("Empty response")
		} else {
			err_out = err
		}
		db_client.releaseDbClient()
		return
	}
	venues_out = new(play.Venues)
	for i := range venues.Venues {
		venues_out.Venues = append(venues_out.Venues, copyVenueBack(venues.Venues[i]))
	}
	db_client.releaseDbClient()
	return
}

func (db_client *PlayDbClient) GetEventsByVenueReq(c context.Context, uuid_in *play.ReqUUID) (events_out *play.Events, err_out error) {

	_db := db_client.getSyncDbClient()
	events, err := _db.GetEventsByVenue(c, copyUUID(uuid_in))
	if err != nil || len(events.Events) == 0 {
		if err == nil {
			err_out = errors.New("Empty response")
		} else {
			err_out = err
		}
		db_client.releaseDbClient()
		return
	}
	events_out = new(play.Events)
	//Only go ahead with successful lookups.
	for i := range events.Events {

		events_out.Events = append(events_out.Events, copyEventBack(events.Events[i]))

		if vn, e := _db.GetVenueById(c, &db.ReqUUID{Uuid:events.Events[i].EventUuid}); e == nil {
			events_out.Events[len(events_out.Events) - 1].Venue = copyVenueBack(vn)
		}

		if org, e := _db.GetOrganizerById(c, &db.ReqUUID{Uuid:events.Events[i].EventUuid}); e == nil {
			events_out.Events[len(events_out.Events) - 1].Org = copyOrgBack(org)
		}

		if ea, e := _db.GetEventAvailabilityInfo(c, &db.ReqUUID{Uuid:events.Events[i].EventUuid}); e == nil {
			events_out.Events[len(events_out.Events) - 1].EaInfo = copyEventAvailabilityBack(ea)
		}

		if ep, e := _db.GetEventPlayersInfo(c, &db.ReqUUID{Uuid:events.Events[i].EventUuid}); e == nil {
			events_out.Events[len(events_out.Events) - 1].EpInfo = copyEventPlayersBack(ep)
		}
	}
	db_client.releaseDbClient()
	return
}

func (db_client *PlayDbClient) CreateEventJoinRequest(c context.Context,
		req_in *play.EventJoinRequest) (req_out *play.EventJoinRequest, err_out error) {

	_db := db_client.getSyncDbClient()
	if out, err := _db.CreateEventJoinReq(c, copyEventJoinReq(req_in)); err != nil {
		err_out = err
	} else {
		req_out = copyEventJoinReqBack(out)
	}
	db_client.releaseDbClient()
	return
}

func (db_client *PlayDbClient) CreateEventInviteReq(c context.Context,
		invite_in *play.EventInvite) (invite_out *play.EventInvite, err_out error) {

	_db := db_client.getSyncDbClient()
	if out, err := _db.CreateEventInvite(c, copyEventInvite(invite_in)); err != nil {
		err_out = err
	} else {
		invite_out = copyEventInviteBack(out)
	}
	db_client.releaseDbClient()
	return
}

// Handle later errors. Have to delete Event otherwise.
func (db_client *PlayDbClient) CreateEventReq(c context.Context,
		event_in *play.Event) (event_out *play.Event, err_out error) {

	_db := db_client.getSyncDbClient()
	if ev, err := _db.CreateEvent(c, copyEvent(event_in)); err == nil {
		event_in.EaInfo.EventUuid = ev.EventUuid
		event_in.EpInfo.EventUuid = ev.EventUuid

		event_out = copyEventBack(ev)
		if eaInfo, e := _db.CreateEventAvailabilityInfo(c, copyEventAvailability(event_in.EaInfo)); e == nil {
			event_out.EaInfo = copyEventAvailabilityBack(eaInfo)
		}
		if epInfo, e := _db.CreateEventPlayersInfo(c, copyEventPlayers(event_in.EpInfo)); e == nil {
			event_out.EpInfo = copyEventPlayersBack(epInfo)
		}
		event_out.Venue = event_in.Venue
		event_out.Org = event_in.Org
	} else {
		err_out = err
	}
	db_client.releaseDbClient()
	return
}

func (db_client *PlayDbClient) CreateVenueReq(c context.Context,
		venue_in *play.Venue) (venue_out *play.Venue, err_out error) {

	_db := db_client.getSyncDbClient()
	if out, err := _db.CreateVenue(c, copyVenue(venue_in)); err != nil {
		err_out = err
	} else {
		venue_out = copyVenueBack(out)
	}
	db_client.releaseDbClient()
	return
}

func (db_client *PlayDbClient) AcceptEventJoinRequest(c context.Context,
		req_in *play.EventJoinRequest) (req_out *play.EventJoinRequest, err_out error) {

	_db := db_client.getSyncDbClient()
	if out, err := _db.CreateEventJoinReq(c, copyEventJoinReq(req_in)); err != nil {
		err_out = err
	} else {
		req_out = copyEventJoinReqBack(out)
	}
	db_client.releaseDbClient()
	return
}
func (db_client *PlayDbClient) AcceptEventInviteReq(c context.Context,
		invite_in *play.EventInvite) (invite_out *play.EventInvite, err_out error) {

	_db := db_client.getSyncDbClient()
	if out, err := _db.CreateEventInvite(c, copyEventInvite(invite_in)); err != nil {
		err_out = err
	} else {
		invite_out = copyEventInviteBack(out)
	}
	db_client.releaseDbClient()
	return
}

// Copy functions for the structures. Creates new structure.
func copyUUID(uuid_in *play.ReqUUID) (uuid_out *db.ReqUUID) {
	uuid_out = new(db.ReqUUID)
	uuid_out.Uuid = uuid_in.Uuid
	return
}

func copyLocInfo(loc_in *play.LocationInfo) (loc_out *db.LocationInfo) {
	loc_out = new (db.LocationInfo)
	loc_out.Latitude = loc_in.Latitude
	loc_out.Longitude = loc_in.Longitude
	loc_out.Radius = loc_in.Radius
	return
}

func copyUserInfo(user_in *play.UserInfo) (user_out *db.UserInfo) {
	user_out = new(db.UserInfo)
	user_out.UserUuid = user_in.UserUuid
	user_out.Name = user_in.Name
	user_out.Email = user_in.Email
	user_out.Mobile = user_in.Mobile
	user_out.GcmRegId = user_in.GcmRegId
	return
}

func copyUserInfoBack(user_in *db.UserInfo) (user_out *play.UserInfo) {
	user_out = new(play.UserInfo)
	user_out.UserUuid = user_in.UserUuid
	user_out.Name = user_in.Name
	user_out.Email = user_in.Email
	user_out.Mobile = user_in.Mobile
	user_out.GcmRegId = user_in.GcmRegId
	return
}

func copyEvent(event_in *play.Event) (event_out *db.Event) {
	event_out = new(db.Event)
	event_out.EventUuid = event_in.EventUuid
	event_out.EventName = event_in.EventName
	event_out.EventUrl = event_in.EventUrl
	event_out.EventType = event_in.EventType
	event_out.EventCat = event_in.EventCat
	event_out.EventStart = event_in.EventStart
	event_out.EventEnd = event_in.EventEnd
	event_out.VenueUuid = event_in.Venue.VenueUuid
	event_out.OrganizerUuid = event_in.Org.OrgUuid
	event_out.DomainId = event_in.EventUuid
	event_out.DomainName = "PLAYY"
	event_out.Description = event_in.Description
	return
}

func copyEventBack(event_in *db.Event) (event_out *play.Event) {
	event_out = new(play.Event)
	event_out.EventUuid = event_in.EventUuid
	event_out.EventName = event_in.EventName
	event_out.EventUrl = event_in.EventUrl
	event_out.EventType = event_in.EventType
	event_out.EventCat = event_in.EventCat
	event_out.EventStart = event_in.EventStart
	event_out.EventEnd = event_in.EventEnd
	event_out.DomainId = event_in.DomainId
	event_out.DomainName = event_out.DomainName
	event_out.Description = event_in.Description
	return
}

func copyVenue(venue_in *play.Venue) (venue_out *db.Venue) {
	venue_out = new(db.Venue)
	venue_out.VenueUuid = venue_in.VenueUuid
	venue_out.VenueName = venue_in.VenueName
	venue_out.VenueUrl = venue_in.VenueUrl
	venue_out.EventCatVenue = venue_in.EventCatVenue
	venue_out.Latitude = venue_in.Latitude
	venue_out.Longitude = venue_in.Longitude
	venue_out.Address_1 = venue_in.Address_1
	venue_out.Address_2 = venue_in.Address_2
	venue_out.City = venue_in.City
	venue_out.State = venue_in.State
	venue_out.Zip = venue_in.Zip
	venue_out.DomainId = venue_in.DomainId
	venue_out.DomainName = venue_in.DomainName
	return
}

func copyVenueBack(venue_in *db.Venue) (venue_out *play.Venue) {
	venue_out = new(play.Venue)
	venue_out.VenueUuid = venue_in.VenueUuid
	venue_out.VenueName = venue_in.VenueName
	venue_out.VenueUrl = venue_in.VenueUrl
	venue_out.EventCatVenue = venue_in.EventCatVenue
	venue_out.Latitude = venue_in.Latitude
	venue_out.Longitude = venue_in.Longitude
	venue_out.Address_1 = venue_in.Address_1
	venue_out.Address_2 = venue_in.Address_2
	venue_out.City = venue_in.City
	venue_out.State = venue_in.State
	venue_out.Zip = venue_in.Zip
	venue_out.DomainId = venue_in.DomainId
	venue_out.DomainName = venue_in.DomainName
	return
}

//func copyOrg(org_in *play.Organizer) (org_out *db.Organizer) {
//	org_out = new(db.Organizer)
//	org_out.OrgUuid = org_in.OrgUuid
//	org_out.OrgName = org_in.OrgName
//	org_out.OrgUrl = org_in.OrgUrl
//	org_out.OrgEmail = org_in.OrgEmail
//	org_out.OrgPh = org_in.OrgPh
//	org_out.OrgContactName = org_in.OrgContactName
//	org_out.UserUuid = org_in.UserUuid
//	return
//}

func copyOrgBack(org_in *db.Organizer) (org_out *play.Organizer) {
	org_out = new(play.Organizer)
	org_out.OrgUuid = org_in.OrgUuid
	org_out.OrgName = org_in.OrgName
	org_out.OrgUrl = org_in.OrgUrl
	org_out.OrgEmail = org_in.OrgEmail
	org_out.OrgPh = org_in.OrgPh
	org_out.OrgContactName = org_in.OrgContactName
	org_out.UserUuid = org_in.UserUuid
	return
}

func copyEventAvailability(ea_in *play.EventAvailabilityInfo) (ea_out *db.EventAvailabilityInfo) {
	ea_out = new(db.EventAvailabilityInfo)
	ea_out.EventUuid = ea_in.EventUuid
	ea_out.Cost = ea_in.Cost
	ea_out.TotalAvailable = ea_in.TotalAvailable
	ea_out.Sold = ea_in.Sold
	ea_out.Available = ea_in.Available
	ea_out.SalesStart = ea_in.SalesStart
	ea_out.SalesEnd = ea_in.SalesEnd
	return
}

func copyEventAvailabilityBack(ea_in *db.EventAvailabilityInfo) (ea_out *play.EventAvailabilityInfo) {
	ea_out = new(play.EventAvailabilityInfo)
	ea_out.EventUuid = ea_in.EventUuid
	ea_out.Cost = ea_in.Cost
	ea_out.TotalAvailable = ea_in.TotalAvailable
	ea_out.Sold = ea_in.Sold
	ea_out.Available = ea_in.Available
	ea_out.SalesStart = ea_in.SalesStart
	ea_out.SalesEnd = ea_in.SalesEnd
	return
}

func copyEventPlayers(ep_in *play.EventPlayersInfo) (ep_out *db.EventPlayersInfo) {
	ep_out = new(db.EventPlayersInfo)
	ep_out.EventUuid = ep_in.EventUuid
	ep_out.MinPlayers = ep_out.MinPlayers
	ep_out.MaxPlayers = ep_out.MaxPlayers
	ep_out.MinAge = ep_out.MinAge
	ep_out.MaxAge = ep_out.MaxAge
	return
}

func copyEventPlayersBack(ep_in *db.EventPlayersInfo) (ep_out *play.EventPlayersInfo) {
	ep_out = new(play.EventPlayersInfo)
	ep_out.EventUuid = ep_in.EventUuid
	ep_out.MinPlayers = ep_out.MinPlayers
	ep_out.MaxPlayers = ep_out.MaxPlayers
	ep_out.MinAge = ep_out.MinAge
	ep_out.MaxAge = ep_out.MaxAge
	return
}

func copyEventJoinReq(req_in *play.EventJoinRequest) (req_out *db.EventJoinRequest) {
	req_out = new(db.EventJoinRequest)
	req_out.EventUuid = req_in.EventUuid
	req_out.OrgUuid = req_in.OrgUuid
	req_out.UserUuid = req_in.UserUuid
	req_out.CreatedAt = req_in.CreatedAt
	req_out.SentAt = req_in.SentAt
	req_out.AcceptedAt = req_in.AcceptedAt
	return
}

func copyEventJoinReqBack(req_in *db.EventJoinRequest) (req_out *play.EventJoinRequest) {
	req_out = new(play.EventJoinRequest)
	req_out.EventUuid = req_in.EventUuid
	req_out.OrgUuid = req_in.OrgUuid
	req_out.UserUuid = req_in.UserUuid
	req_out.CreatedAt = req_in.CreatedAt
	req_out.SentAt = req_in.SentAt
	req_out.AcceptedAt = req_in.AcceptedAt
	return
}

func copyEventInvite(invite_in *play.EventInvite) (invite_out *db.EventInvite) {
	invite_out = new(db.EventInvite)
	invite_out.EventUuid = invite_in.EventUuid
	invite_out.UserUuid = invite_in.UserUuid
	invite_out.InviteCode = invite_in.InviteCode
	invite_out.AccUserUuid = invite_in.AccUserUuid
	invite_out.CreatedAt = invite_in.CreatedAt
	invite_out.SentAt = invite_in.SentAt
	invite_out.AcceptedAt = invite_in.AcceptedAt
	return
}

func copyEventInviteBack(invite_in *db.EventInvite) (invite_out *play.EventInvite) {
	invite_out = new(play.EventInvite)
	invite_out.EventUuid = invite_in.EventUuid
	invite_out.UserUuid = invite_in.UserUuid
	invite_out.InviteCode = invite_in.InviteCode
	invite_out.AccUserUuid = invite_in.AccUserUuid
	invite_out.CreatedAt = invite_in.CreatedAt
	invite_out.SentAt = invite_in.SentAt
	invite_out.AcceptedAt = invite_in.AcceptedAt
	return
}
