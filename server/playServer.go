package main

import (
	play "playyServer"
	"golang.org/x/net/context"
	"flag"
	pdb "playyServer/dbClient"
	"net"
	"fmt"
	"google.golang.org/grpc/grpclog"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc"
	"errors"
	"log"
	"playyServer/utils"
	validator "github.com/asaskevich/govalidator"
	"github.com/kellydunn/golang-geo"
	"strconv"
)

const INCORRECT_REQ = "Incorrect request"

var (
	tls        = flag.Bool("tls", false, "Connection uses TLS if true, else plain TCP")
	certFile   = flag.String("cert_file", "testdata/server1.pem", "The TLS cert file")
	keyFile    = flag.String("key_file", "testdata/server1.key", "The TLS key file")
	port       = flag.Int("port", 10000, "The server port")
)

type playServiceServer struct  {
	server *play.PlayServiceServer
	dbClient *pdb.PlayDbClient
}

func newServer() *playServiceServer {
	s := new(playServiceServer)
	s.server = new(play.PlayServiceServer)
	s.dbClient = pdb.NewPlayDbClient()
	return s
}

// Input validation
func ValidateInput(in interface{}) error {

	switch t := in.(type) {
	default:
		log.Fatal("USO!%v", t)

	case *play.ReqUUID:
		if !validator.IsUUID(in.(*play.ReqUUID).Uuid) {
			return GetAppErr(INCORRECT_REQ, in)
		}
		break
	case *play.EventJoinRequest:
		if !validator.IsUUID(in.(*play.EventJoinRequest).EventUuid) ||
			!validator.IsUUID(in.(*play.EventJoinRequest).OrgUuid) ||
			!validator.IsUUID(in.(*play.EventJoinRequest).UserUuid) {

			return GetAppErr(INCORRECT_REQ, in)
		}
		break
	case *play.EventInvite:
		if !validator.IsUUID(in.(*play.EventInvite).EventUuid) ||
			!validator.IsUUID(in.(*play.EventInvite).UserUuid) {

			return GetAppErr(INCORRECT_REQ, in)
		}
		break
	case *play.LocationInfo:
		if !validator.IsLatitude(in.(*play.LocationInfo).Latitude) ||
			!validator.IsLongitude(in.(*play.LocationInfo).Longitude) {

			return GetAppErr(INCORRECT_REQ, in)
		}
		break
	case *play.Event:
		if !ValidateEvent(in.(*play.Event)) {
			return GetAppErr(INCORRECT_REQ, in)
		}
		break
	case *play.Venue:
		if !ValidateVenue(in.(*play.Venue)) {
			return GetAppErr(INCORRECT_REQ, in)
		}
		break
	case *play.UserInfo:
		if (!validator.IsEmail(in.(*play.UserInfo).Email) && !validator.IsNumeric(validator.Trim(in.(*play.UserInfo).Mobile, "+"))) ||
			validator.IsNull(in.(*play.UserInfo).Name) ||
			!validator.IsAlpha(in.(*play.UserInfo).Name) ||
			validator.IsNull(in.(*play.UserInfo).GcmRegId) {

			return GetAppErr(INCORRECT_REQ, in)
		}
		break
	case *play.Organizer:
		if (!validator.IsEmail(in.(*play.Organizer).OrgEmail) && !validator.IsNumeric(validator.Trim(in.(*play.Organizer).OrgPh, "+"))) ||
			validator.IsNull(in.(*play.Organizer).OrgName) ||
			!validator.IsAlpha(in.(*play.Organizer).OrgName) {

			return GetAppErr(INCORRECT_REQ, in)
		}
		break
	}

	return nil
}

func ValidateEvent(event *play.Event) bool {
	return true
}

func ValidateVenue(venue *play.Venue) bool {
	return true
}

func GetAppErr(err_str string, arg interface{}) error {
	log.Println("Error: %v Func: %v Arg: %v", err_str, utils.MyCaller(), arg)
	return errors.New(err_str)
}

func (s *playServiceServer) Login(c context.Context, user_in *play.UserInfo) (user_out *play.UserInfo, err_out error) {

	if (len(user_in.Email) == 0 && len(user_in.Mobile) == 0) ||
		(!validator.IsEmail(user_in.Email) && !validator.IsNumeric(validator.Trim(user_in.Mobile, "+"))) {
		err_out = GetAppErr(INCORRECT_REQ, user_in)
	}

	user_out, err_out = s.dbClient.GetUserByInfoReq(c, user_in)

	if err_out != nil {
		_ = GetAppErr(err_out.Error(), user_in)
	}

	return
}

func (s *playServiceServer) CreateProfile(c context.Context, user_in *play.UserInfo) (user_out *play.UserInfo, err_out error) {

	if err_out = ValidateInput(user_in); err_out != nil {
		return
	}

	user_out, err_out = s.dbClient.CreateUserReq(c, user_in)

	if err_out != nil {
		_ = GetAppErr(err_out.Error(), user_in)
	}

	return
}

func (s *playServiceServer) GetEventsByLocation(c context.Context, loc_in *play.LocationInfo) (results_out *play.ResultsByVenue, err_out error) {

	if err_out = ValidateInput(loc_in); err_out != nil {
		return
	}

	venues, err := s.GetVenuesByLocation(c, loc_in)

	if err != nil || len(venues.Venues) == 0 {
		if err == nil {
			err_out = errors.New("Empty response")
		} else {
			err_out = err
		}
		_ = GetAppErr(err_out.Error(), loc_in)
		return
	} else {
		results_out = new(play.ResultsByVenue)
		results_out.ResultsByVenue = make([]*play.EventsByVenue, 1)
	}

	for v := range venues.Venues {
		ev := new(play.EventsByVenue)
		ev.Venue = venues.Venues[v]
		// Not checking error right now.
		ev.Events, _ = s.GetEventsByVenue(c, &play.ReqUUID{Uuid:ev.Venue.VenueUuid})
		results_out.ResultsByVenue = append(results_out.ResultsByVenue, ev)
	}

	if err_out != nil {
		_ = GetAppErr(err_out.Error(), loc_in)
	}

	return
}

func (s *playServiceServer) GetMyEvents(c context.Context, user_in *play.ReqUUID) (events_out *play.Events, err_out error) {

	if err_out = ValidateInput(user_in); err_out != nil {
		return
	}

	events_out, err_out = s.dbClient.GetMyEventsReq(c, user_in)

	if err_out != nil {
		_ = GetAppErr(err_out.Error(), user_in)
	}

	return
}

func (s *playServiceServer) GetEventsByVenue(c context.Context, venue_in *play.ReqUUID) (events_out *play.Events, err_out error) {

	if err_out = ValidateInput(venue_in); err_out != nil {
		return
	}

	events_out, err_out = s.dbClient.GetEventsByVenueReq(c, venue_in)

	if err_out != nil {
		_ = GetAppErr(err_out.Error(), venue_in)
	}

	return
}

func (s * playServiceServer) CreateEvent(c context.Context, event_in *play.Event) (event_out *play.Event, err_out error) {

	if err_out = ValidateInput(event_in); err_out != nil {
		return
	}

	event_out, err_out = s.dbClient.CreateEventReq(c, event_in)

	if err_out != nil {
		_ = GetAppErr(err_out.Error(), event_in)
	}

	return
}

func (s *playServiceServer) GetVenuesByLocation(c context.Context,
		loc_in *play.LocationInfo) (venues_out *play.Venues, err_out error) {

	if err_out = ValidateInput(loc_in); err_out != nil {
		return
	}

	venues_out, err_out = s.dbClient.GetVenuesByLocationReq(c, loc_in)

	if err_out != nil {
		_ = GetAppErr(err_out.Error(), loc_in)
	} else {
		lat, _ := strconv.ParseFloat(loc_in.Latitude, 64)
		lon, _ := strconv.ParseFloat(loc_in.Longitude, 64)
		p1 := geo.NewPoint(lat, lon)

		for i := range venues_out.Venues {
			lat2, _ := strconv.ParseFloat(venues_out.Venues[i].Latitude, 64)
			lon2, _ := strconv.ParseFloat(venues_out.Venues[i].Longitude, 64)
			p2 := geo.NewPoint(lat2, lon2)

			distance := utils.CalcDistance(p1, p2)
			venues_out.Venues[i].CurrDistance = uint32(distance)
		}
	}

	return
}

func (s * playServiceServer) CreateVenue(c context.Context, venue_in *play.Venue) (venue_out *play.Venue, err_out error) {

	if err_out = ValidateInput(venue_in); err_out != nil {
		return
	}

	venue_out, err_out = s.dbClient.CreateVenueReq(c, venue_in)

	if err_out != nil {
		_ = GetAppErr(err_out.Error(), venue_in)
	}

	return
}

func (s * playServiceServer) JoinEvent(c context.Context, req_in *play.EventJoinRequest) (req_out *play.EventJoinRequest, err_out error) {

	if err_out = ValidateInput(req_in); err_out != nil {
		return
	}

	req_out, err_out = s.dbClient.CreateEventJoinRequest(c, req_in)

	if err_out != nil {
		_ = GetAppErr(err_out.Error(), req_in)
	}
	
	return
}

func (s * playServiceServer) CreateInvite(c context.Context, invite_in *play.EventInvite) (invite_out *play.EventInvite, err_out error) {

	if err_out = ValidateInput(invite_in); err_out != nil {
		return
	}
	invite_in.InviteCode = utils.RandStringBytes(40)

	invite_out, err_out = s.dbClient.CreateEventInviteReq(c, invite_in)

	if err_out != nil {
		_ = GetAppErr(err_out.Error(), invite_in)
	}

	return
}

func (s * playServiceServer) AcceptJoinRequest(c context.Context, req_in *play.EventJoinRequest) (req_out *play.EventJoinRequest, err_out error) {

	if err_out = ValidateInput(req_in); err_out != nil {
		return
	}

	req_out, err_out = s.dbClient.AcceptEventJoinRequest(c, req_in)

	if err_out != nil {
		_ = GetAppErr(err_out.Error(), req_in)
	}

	return
}

func (s * playServiceServer) AcceptEventInvite(c context.Context, invite_in *play.EventInvite) (invite_out *play.EventInvite, err_out error) {

	if err_out = ValidateInput(invite_in); err_out != nil {
		return
	}

	if err_out = ValidateInput(&play.ReqUUID{Uuid:invite_in.AccUserUuid}); err_out != nil {
		 return
	}

	invite_out, err_out = s.dbClient.AcceptEventInviteReq(c, invite_in)

	if err_out != nil {
		_ = GetAppErr(err_out.Error(), invite_in)
	}

	return
}

func (s *playServiceServer) GetHeartbeat(c context.Context, emp *play.EmptyMessage) (*play.EmptyMessage, error)  {
	//HeartBeat query
	return &play.EmptyMessage{}, nil
}

func main() {
	flag.Parse()
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		grpclog.Fatalf("failed to listen: %v", err)
	}
	var opts []grpc.ServerOption
	if *tls {
		creds, err := credentials.NewServerTLSFromFile(*certFile, *keyFile)
		if err != nil {
			grpclog.Fatalf("Failed to generate credentials %v", err)
		}
		opts = []grpc.ServerOption{grpc.Creds(creds)}
	}
	grpcServer := grpc.NewServer(opts...)
	play.RegisterPlayServiceServer(grpcServer, newServer())
	grpcServer.Serve(lis)
}
